export interface Inote {
  id: string;
  title: string;
  text: string;
  color: string;
  date: string;
}
