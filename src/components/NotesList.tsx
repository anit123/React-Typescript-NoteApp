import * as React from 'react';
import { Inote } from '../models/note.model';
import Notes from './Notes';

interface INotesListProps {
  notes: Inote[];
  setNote: React.Dispatch<React.SetStateAction<Inote[]>>;
}

const NotesList: React.FunctionComponent<INotesListProps> = ({
  notes,
  setNote,
}) => {
  const handleDelete = (id: string) => {
    setNote(notes.filter((note) => note.id !== id));
  };
  const renderNotes = (): JSX.Element[] => {
    return notes.map((note) => {
      return <Notes key={note.id} note={note} handleDelete={handleDelete} />;
    });
  };
  return (
    <>
      <h2 className="mt-3">Notes</h2>
      <div>{renderNotes()}</div>
    </>
  );
};

export default NotesList;
