import React, { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './App.css';
import CreateNote from './components/CreateNote';
import Header from './components/Header';
import NotesList from './components/NotesList';
import { Inote } from './models/note.model';

function App() {
  const [notes, setNotes] = useState<Inote[]>([
    {
      id: new Date().toString(),
      title: 'My first note',
      text: 'I am going to add my first noet, this is awesome!!',
      color: '#dfdfdf',
      date: new Date().toString(),
    },
  ]);
  return (
    <div className="App">
      <Header />
      <Container className="mt-5">
        <Row>
          <Col>
            <NotesList notes={notes} setNote={setNotes} />
          </Col>
        </Row>
        <Row>
          <Col>
            <CreateNote notes={notes} setNote={setNotes} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
